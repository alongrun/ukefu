package com.ukefu.webim.web.handler.admin.system;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.util.UKeFuList;
import com.ukefu.util.es.SearchTools;
import com.ukefu.util.es.UKDataBean;
import com.ukefu.util.metadata.DatabaseMetaDataHandler;
import com.ukefu.util.metadata.UKColumnMetadata;
import com.ukefu.util.metadata.UKTableMetaData;
import com.ukefu.webim.service.hibernate.BaseService;
import com.ukefu.webim.service.impl.ESDataExchangeImpl;
import com.ukefu.webim.service.repository.MetadataRepository;
import com.ukefu.webim.service.repository.SysDicRepository;
import com.ukefu.webim.service.repository.TablePropertiesRepository;
import com.ukefu.webim.service.repository.TopicItemRepository;
import com.ukefu.webim.service.repository.XiaoETopiccRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.MetadataTable;
import com.ukefu.webim.web.model.SysDic;
import com.ukefu.webim.web.model.TableProperties;
import com.ukefu.webim.web.model.Topic;
import com.ukefu.webim.web.model.TopicItem;
import com.ukefu.webim.web.model.UKeFuDic;
import com.ukefu.webim.web.model.User;

@Controller
@RequestMapping("/admin/metadata")
public class MetadataController extends Handler{
	
	@Autowired
	private MetadataRepository metadataRes ;
	
	@Autowired
	private TopicItemRepository topicItemRes ;
	
	@Autowired
	private BaseService<?> service ;
	
	@Autowired
	private SysDicRepository sysDicRes ;
	
	@Autowired
	private TablePropertiesRepository tablePropertiesRes ;
	
	@Autowired
	@PersistenceContext
	private EntityManager em;
	
	@Autowired
	private ESDataExchangeImpl esDataExchange ;
	
	/**
	 * 	普通元数据 1
	 * @param map
	 * @param request
	 * @param msg
	 * @param tabletype
	 * @return
	 * @throws SQLException
	 */
    @RequestMapping("/index")
    @Menu(type = "admin" , subtype = "metadata" , name="db", admin = true)
    public ModelAndView index(ModelMap map , HttpServletRequest request ,String msg ,String tabletype) throws SQLException {
    	this.serchMetadateList(map, request, tabletype,msg);
        return request(super.createAdminTempletResponse("/admin/system/metadata/detail/index"));
    }
    /**
     * 	导入SQL 2
     * @param map
     * @param request
     * @param msg
     * @param tabletype
     * @return
     * @throws SQLException
     */
    @RequestMapping("/sqltable/list")
    @Menu(type = "admin" , subtype = "metadata" , name="sql", admin = true)
    public ModelAndView searchSQLTable(ModelMap map , HttpServletRequest request ,String msg ,String tabletype) throws SQLException {
    	this.serchMetadateList(map, request, tabletype,msg);
    	return request(super.createAdminTempletResponse("/admin/system/metadata/detail/sqltable"));
    }
    /**
     * 	ES数据表，电销批次导入的数据结构 3
     * @param map
     * @param request
     * @param msg
     * @param tabletype
     * @return
     * @throws SQLException
     */
    @RequestMapping("/estable/list")
    @Menu(type = "admin" , subtype = "metadata" , name="es", admin = true)
    public ModelAndView searchESTable(ModelMap map , HttpServletRequest request ,String msg ,String tabletype) throws SQLException {
    	this.serchMetadateList(map, request, tabletype,msg);
    	return request(super.createAdminTempletResponse("/admin/system/metadata/detail/estable"));
    }
    /**
     * 	CRM数据表 4
     * @param map
     * @param request
     * @param msg
     * @param tabletype
     * @return
     * @throws SQLException
     */
    @RequestMapping("/crmtable/list")
    @Menu(type = "admin" , subtype = "metadata" , name="crm", admin = true)
    public ModelAndView searchCRMTable(ModelMap map , HttpServletRequest request ,String msg ,String tabletype) throws SQLException {
    	this.serchMetadateList(map, request, tabletype,msg);
    	return request(super.createAdminTempletResponse("/admin/system/metadata/detail/crmtable"));
    }
   
    /**
     * 	根据元数据类型查询元数据
     * @param map
     * @param request
     * @param tabletype
     * @param msg
     */
    public void serchMetadateList(ModelMap map ,HttpServletRequest request ,final String tabletype ,String msg) {
    	final String orgi = super.getOrgi(request);
    	Page<MetadataTable> metadataList = this.metadataRes.findAll(new Specification<MetadataTable>() {
    		@Override
			public Predicate toPredicate(Root<MetadataTable> root, CriteriaQuery<?> query,
					CriteriaBuilder cb) {
    			List<Predicate> list = new ArrayList<Predicate>();  
    			list.add(cb.equal(root.get("orgi").as(String.class), orgi));
    			if(!org.apache.commons.lang3.StringUtils.isBlank(tabletype)) {
    				list.add(cb.equal(root.get("tabletype").as(String.class), tabletype));
    			}else {
    				list.add(cb.equal(root.get("tabletype").as(String.class), "1"));
    			}
    			Predicate[] p = new Predicate[list.size()];  
				return cb.and(list.toArray(p));  
    		}
		}, new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC,new String[] {"createtime"}));
    	map.addAttribute("metadataList", metadataList);
    	map.addAttribute("msg", msg);
    	map.addAttribute("tabletype", tabletype);
    }
    
    @RequestMapping("/edit")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView edit(ModelMap map , HttpServletRequest request , @Valid String id ,String tabletype) {
    	map.addAttribute("metadata", metadataRes.findById(id)) ;
    	map.addAttribute("propertiesList", tablePropertiesRes.findByDbtableid(id)) ;
    	map.addAttribute("tabletype", tabletype);
    	return request(super.createRequestPageTempletResponse("/admin/system/metadata/edit"));
    }
    
    @RequestMapping("/update")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView update(ModelMap map , HttpServletRequest request , @Valid MetadataTable metadata,String tabletype) throws SQLException {
    	MetadataTable table = metadataRes.findById(metadata.getId()) ;
    	table.setName(metadata.getName());
    	table.setFromdb(metadata.isFromdb());
    	if(metadata.isFromdb()) {
    		table.setTabletype("3");
    	}else {
    		table.setTabletype("1");
    	}
    	table.setListblocktemplet(metadata.getListblocktemplet());
    	table.setPreviewtemplet(metadata.getPreviewtemplet());
    	metadataRes.save(table);
    	return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/index.html?tabletype="+tabletype));
    }
    
    @RequestMapping("/addsql")
    @Menu(type = "admin" , subtype = "addsql" , admin = true)
    public ModelAndView addsql(ModelMap map , HttpServletRequest request,String tabletype) {
    	map.addAttribute("tabletype", tabletype);
    	return request(super.createRequestPageTempletResponse("/admin/system/metadata/addsql"));
    }
    @RequestMapping("/addsqlsave")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView addsqlsave(ModelMap map , HttpServletRequest request , final @Valid String datasql , final @Valid String name,String tabletype) throws Exception {
    	if(!StringUtils.isBlank(datasql) && !StringUtils.isBlank(name)){
    		final User user = super.getUser(request);
	    	Session session = (Session) em.getDelegate();
	    	session.doWork(
	    		    new Work() {
	    		        public void execute(Connection connection) throws SQLException 
	    		        {
	    		        	try{
	    		        		int count = metadataRes.countByTablename(name) ;
    				    		if(count == 0){
    			 		    		MetadataTable metaDataTable = new MetadataTable();
    				  				//当前记录没有被添加过，进行正常添加
    				  				metaDataTable.setTablename(name);
    				  				metaDataTable.setOrgi(user.getOrgi());
    				  				metaDataTable.setId(UKTools.md5(metaDataTable.getTablename()));
    				  				metaDataTable.setTabledirid("0");
    				  				metaDataTable.setCreater(user.getId());
    				  				metaDataTable.setCreatername(user.getUsername());
    				  				metaDataTable.setName(name);
    				  				metaDataTable.setDatasql(datasql);
    				  				metaDataTable.setTabletype("2");
    				  				metaDataTable.setUpdatetime(new Date());
    				  				metaDataTable.setCreatetime(new Date());
    				  				metadataRes.save(processMetadataTable( DatabaseMetaDataHandler.getSQL(connection, name, datasql) , metaDataTable));
    				    		}
	    			    	}catch(Exception ex){
	    			    		ex.printStackTrace();
	    			    	}finally{
	    			    		connection.close();
	    			    	}
	    		        }
	    		    }
	    		);
	    	
    	}
    	
        return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/sqltable/list.html?tabletype="+tabletype));
    }
    
    @RequestMapping("/editsql")
    @Menu(type = "admin" , subtype = "editsql" , admin = true)
    public ModelAndView editsql(ModelMap map , HttpServletRequest request , @Valid String id ,@Valid String tabletype) {
    	map.addAttribute("metadata", metadataRes.findById(id)) ;
    	map.addAttribute("tabletype", tabletype);
    	return request(super.createRequestPageTempletResponse("/admin/system/metadata/editsql"));
    }
    
    

    @RequestMapping("/updatesql")
    @Menu(type = "admin" , subtype = "updatesql" , admin = true)
    public ModelAndView updatesql(ModelMap map , HttpServletRequest request , @Valid MetadataTable metadata ,@Valid String tabletype) throws SQLException {
    	MetadataTable table = metadataRes.findById(metadata.getId()) ;
    	if(table!=null) {
	    	table.setName(metadata.getName());
	    	table.setDatasql(metadata.getDatasql());
	    	metadataRes.save(table);
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/sqltable/list.html?tabletype="+tabletype));
    }
    
    @RequestMapping("/properties/edit")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView propertiesedit(ModelMap map , HttpServletRequest request , @Valid String id) {
    	map.addAttribute("tp", tablePropertiesRes.findById(id)) ;
    	map.addAttribute("sysdicList", sysDicRes.findByParentid("0")) ;
    	map.addAttribute("dataImplList", UKeFuDic.getInstance().getDic("com.dic.data.impl")) ;
    	
    	return request(super.createRequestPageTempletResponse("/admin/system/metadata/tpedit"));
    }
    
    @RequestMapping("/properties/update")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView propertiesupdate(ModelMap map , HttpServletRequest request , @Valid TableProperties tp) throws SQLException {
    	TableProperties tableProperties = tablePropertiesRes.findById(tp.getId()) ;
    	tableProperties.setName(tp.getName());
    	tableProperties.setSeldata(tp.isSeldata());
    	tableProperties.setSeldatacode(tp.getSeldatacode());
    	
    	tableProperties.setReffk(tp.isReffk());
    	tableProperties.setReftbid(tp.getReftbid());
    	
    	tableProperties.setDefaultvaluetitle(tp.getDefaultvaluetitle());
    	tableProperties.setDefaultfieldvalue(tp.getDefaultfieldvalue());
    	
    	tableProperties.setModits(tp.isModits());
    	tableProperties.setPk(tp.isPk()) ;
    	
    	tableProperties.setSystemfield(tp.isSystemfield());
    	
    	tableProperties.setPlugin(tp.getPlugin());
    	
    	tableProperties.setImpfield(tp.isImpfield());
    	
    	tableProperties.setSortindex(tp.getSortindex());
    	tableProperties.setViewtype(tp.getViewtype());
    	tablePropertiesRes.save(tableProperties);
    	return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table.html?id="+tableProperties.getDbtableid()));
    }
    @RequestMapping("/properties/update/mul")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView editMultiple(ModelMap map , HttpServletRequest request , @Valid String[] ids, @Valid String tbid) {
    	String[] porids = ids;
    	map.addAttribute("porids", porids);
    	map.addAttribute("tbid", tbid);
    	return request(super.createRequestPageTempletResponse("/admin/system/metadata/editmultiple"));
    }
    
    @RequestMapping("/properties/update/mul/save")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView updateMultiple(ModelMap map , HttpServletRequest request , @Valid TableProperties table, @Valid String[] porids, @Valid String tbid) throws SQLException {
    	
    	if(porids != null && porids.length > 0) {
    		List<TableProperties> proList = new ArrayList<TableProperties>();
    		for(String proid : porids) {
        		TableProperties tableProperties = tablePropertiesRes.findById(proid) ;
        		if(tableProperties != null) {
        			tableProperties.setImpfield(table.isImpfield());
        			tableProperties.setViewtype(table.getViewtype());
        			proList.add(tableProperties);
        		}
        	}
    		if(proList.size() > 0) {
    			tablePropertiesRes.save(proList);
    		}
    	}
    	
    	return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table.html?id="+tbid));
    }
    @RequestMapping("/delete")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView delete(ModelMap map , HttpServletRequest request , @Valid String id ,@Valid String tabletype) throws SQLException {
    	MetadataTable table = metadataRes.findById(id) ;
    	metadataRes.delete(table);
    	ModelAndView view = new ModelAndView();
    	//普通元数据
    	view = request(super.createRequestPageTempletResponse("redirect:/admin/metadata/index.html?tabletype="+tabletype));
    	if(!org.apache.commons.lang3.StringUtils.isBlank(tabletype)) {
    		if(("2").equals(tabletype)) {//SQL
    			view = request(super.createRequestPageTempletResponse("redirect:/admin/metadata/sqltable/list.html?tabletype="+tabletype));
    		}else if(("3").equals(tabletype)) {//ES，电销批次导入的名单
    			view = request(super.createRequestPageTempletResponse("redirect:/admin/metadata/estable/list.html?tabletype="+tabletype));
    		}
    	}
    	return view;
    }
    
    @RequestMapping("/sync")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView sync(ModelMap map , HttpServletRequest request , @Valid String id, @Valid String tabletype) throws SQLException {
    	final MetadataTable table = metadataRes.findById(id) ;
    	Session session = (Session) em.getDelegate();
		session.doWork(new Work() {
			public void execute(Connection connection) throws SQLException {
				try {
					MetadataTable metaDataTable = new MetadataTable();
					//当前记录没有被添加过，进行正常添加
	  				metaDataTable.setTablename(table.getTablename());
	  				metaDataTable.setOrgi(table.getOrgi());
	  				metaDataTable.setId(UKTools.md5(metaDataTable.getTablename()));
	  				metaDataTable.setTabledirid("0");
	  				metaDataTable.setCreater(table.getCreater());
	  				metaDataTable.setCreatername(table.getCreatername());
	  				metaDataTable.setName(table.getName());
	  				metaDataTable.setUpdatetime(new Date());
	  				metaDataTable.setCreatetime(new Date());
					processMetadataTable( DatabaseMetaDataHandler.getTable(connection, metaDataTable.getTablename()) , metaDataTable) ;
					for(TableProperties temp : metaDataTable.getTableproperty()) {
						boolean found = false ;
						for(TableProperties tp : table.getTableproperty()) {
							if(temp.getFieldname().equals(tp.getFieldname())) {
								found = true ;
								break ;
							}
						}
						if(found == false) {
							temp.setDbtableid(table.getId());
							tablePropertiesRes.save(temp) ;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
		    		connection.close();
		    	}
			}
		});
    	return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table.html?id="+id+"&tabletype="+tabletype));
    }
    
    @RequestMapping("/batdelete")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView batdelete(ModelMap map , HttpServletRequest request , @Valid String[] ids ,@Valid String tabletype) throws SQLException {
    	String msg = "bat_delete_success";
    	if(ids!=null && ids.length>0){
    		metadataRes.delete(metadataRes.findAll(Arrays.asList(ids)) );
    	}else {
    		msg = "bat_delete_faild";
    	}
    	ModelAndView view = new ModelAndView();
    	//普通元数据
    	view = request(super.createRequestPageTempletResponse("redirect:/admin/metadata/index.html?tabletype="+tabletype));
    	if(!org.apache.commons.lang3.StringUtils.isBlank(tabletype)) {
    		if(("2").equals(tabletype)) {//SQL
    			view = request(super.createRequestPageTempletResponse("redirect:/admin/metadata/sqltable/list.html?tabletype="+tabletype));
    		}else if(("3").equals(tabletype)) {//ES，电销批次导入的名单
    			view = request(super.createRequestPageTempletResponse("redirect:/admin/metadata/estable/list.html?tabletype="+tabletype));
    		}
    	}
    	map.addAttribute("msg", msg);
    	return view;
    }
    
    @RequestMapping("/properties/delete")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView propertiesdelete(ModelMap map , HttpServletRequest request , @Valid String id , @Valid String tbid) throws SQLException {
    	TableProperties prop = tablePropertiesRes.findById(id) ;
    	tablePropertiesRes.delete(prop);
        return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table.html?id="+ (!StringUtils.isBlank(tbid) ? tbid : prop.getDbtableid())));
    }
    
    @RequestMapping("/properties/batdelete")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView propertiesbatdelete(ModelMap map , HttpServletRequest request , @Valid String[] ids, @Valid String tbid) throws SQLException {
    	String msg = "bat_delete_success";
    	if(ids!=null && ids.length>0){
    		tablePropertiesRes.delete(tablePropertiesRes.findAll(Arrays.asList(ids)) );
    	}else {
    		msg = "bat_delete_faild";
    	}
        return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table.html?id="+ tbid+"&msg="+msg));
    }
    
    @RequestMapping("/table")
    @Menu(type = "admin" , subtype = "metadata" , name="structure", admin = true)
    public ModelAndView table(ModelMap map , HttpServletRequest request , @Valid String id, @Valid String msg, @Valid String tabletype) throws SQLException {
    	map.addAttribute("propertiesList", tablePropertiesRes.findByDbtableid(id)) ;
    	map.addAttribute("tbid", id) ;
    	map.addAttribute("table", metadataRes.findById(id)) ;
    	map.addAttribute("msg", msg);
    	map.addAttribute("tabletype", tabletype);
        return request(super.createAdminTempletResponse("/admin/system/metadata/table/tableprolist"));
    }
    
    @RequestMapping("/table/data")
    @Menu(type = "admin" , subtype = "metadata" ,name="data", admin = true)
    public ModelAndView tableData(ModelMap map , HttpServletRequest request , @Valid String id, @Valid String msg, @Valid String tabletype) throws SQLException, Exception {
    	map.addAttribute("propertiesList", tablePropertiesRes.findByDbtableid(id)) ;
    	map.addAttribute("tbid", id) ;
    	MetadataTable table = metadataRes.findById(id);
    	map.addAttribute("table", table) ;
    	map.addAttribute("tabletype", tabletype);
    	if(table != null) {
    		if(table.isFromdb()) {
    			//查ES数据
    			BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
    			PageImpl<UKDataBean> dataList = SearchTools.findAllPageMetadata(queryBuilder, new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.DESC, new String[] { "createtime" }) , table.getTablename().toLowerCase());
    			map.addAttribute("dataList", dataList);
    		}
    	}
    	map.addAttribute("msg", msg);
    	return request(super.createAdminTempletResponse("/admin/system/metadata/table/data"));
    }
    
    @RequestMapping("/imptb")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView imptb(final ModelMap map , HttpServletRequest request) throws Exception {
    	this.search(map, request);
    	Session session = (Session) em.getDelegate();
		session.doWork(new Work() {
			public void execute(Connection connection) throws SQLException {
				try {
					map.addAttribute("tablesList",
							DatabaseMetaDataHandler.getTables(connection));
				} catch (Exception e) {
					e.printStackTrace();
				}finally{
		    		connection.close();
		    	}
			}
		});
		
		return request(super
				.createRequestPageTempletResponse("/admin/system/metadata/tablelist"));
    }
    
    public void search(ModelMap map ,HttpServletRequest request) {
    	List<MetadataTable> metaList = metadataRes.findByOrgi(super.getOrgi(request));
		if(metaList != null && !metaList.isEmpty()) {
			StringBuffer str = new StringBuffer();
			for(MetadataTable MetadataTable : metaList) {
				str.append(MetadataTable.getName());
				str.append(",");
			}
			map.addAttribute("metadataTable", str);
		}
    }
    
    @RequestMapping("/imptbsave")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView imptb(ModelMap map , HttpServletRequest request , final @Valid String[] tables) throws Exception {
    	final User user = super.getUser(request) ;
    	if(tables!=null && tables.length > 0){
	    	Session session = (Session) em.getDelegate();
	    	session.doWork(
	    		    new Work() {
	    		        public void execute(Connection connection) throws SQLException 
	    		        {
	    		        	try{
	    				    	for(String table : tables){
	    				    		int count = metadataRes.countByTablename(table) ;
	    				    		if(count == 0){
	    			 		    		MetadataTable metaDataTable = new MetadataTable();
	    				  				//当前记录没有被添加过，进行正常添加
	    				  				metaDataTable.setTablename(table);
	    				  				metaDataTable.setOrgi(user.getOrgi());
	    				  				metaDataTable.setId(UKTools.md5(metaDataTable.getTablename()));
	    				  				metaDataTable.setTabledirid("0");
	    				  				metaDataTable.setCreater(user.getId());
	    				  				metaDataTable.setCreatername(user.getUsername());
	    				  				metaDataTable.setName(table);
	    				  				metaDataTable.setUpdatetime(new Date());
	    				  				metaDataTable.setCreatetime(new Date());
	    				  				metadataRes.save(processMetadataTable( DatabaseMetaDataHandler.getTable(connection, metaDataTable.getTablename()) , metaDataTable));
	    				    		}
	    				    	}
	    			    	}catch(Exception ex){
	    			    		ex.printStackTrace();
	    			    	}finally{
	    			    		connection.close();
	    			    	}
	    		        }
	    		    }
	    		);
	    	
    	}
    	
        return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/index.html"));
    }
    
    public MetadataTable processMetadataTable(UKTableMetaData metaData , MetadataTable table){
    	table.setTableproperty(new ArrayList<TableProperties>()); 
    	if(metaData!=null){
	    	for(UKColumnMetadata colum : metaData.getColumnMetadatas()){
	    		TableProperties tablePorperties = new TableProperties(colum.getName() , colum.getTypeName() , colum.getColumnSize() , metaData.getName()) ;
				tablePorperties.setOrgi(table.getOrgi()) ;
				tablePorperties.setViewtype("list,add,edit,detail");
				tablePorperties.setDatatypecode(0);
				tablePorperties.setLength(colum.getColumnSize());
				tablePorperties.setDatatypename(getDataTypeName(colum.getTypeName()));
				tablePorperties.setName(colum.getTitle());
				if(tablePorperties.getFieldname().toLowerCase().equals("create_time") || tablePorperties.getFieldname().toLowerCase().equals("createtime") || tablePorperties.getFieldname().toLowerCase().equals("update_time")){
					tablePorperties.setDatatypename(getDataTypeName("datetime"));
				}
				if(colum.getName().startsWith("field")){
					tablePorperties.setFieldstatus(false);
				}else{
					tablePorperties.setFieldstatus(true);
				}
				table.getTableproperty().add(tablePorperties) ;
			}
	    	table.setTablename(table.getTablename());//转小写
    	}
    	return table ;
    }
    
    public String getDataTypeName(String type){
    	String typeName = "text" ;
    	if(type.indexOf("varchar")>=0){
    		typeName = "text" ;
    	}else if(type.equalsIgnoreCase("date") || type.equalsIgnoreCase("datetime")){
    		typeName = type.toLowerCase() ;
    	}else if(type.equalsIgnoreCase("int") || type.equalsIgnoreCase("float")  || type.equalsIgnoreCase("number")){
    		typeName = "number" ;
    	}
    	return typeName ;
    }
    
    @RequestMapping("/clean")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView clean(ModelMap map , HttpServletRequest request,@Valid String id,@Valid String tabletype) throws SQLException, BeansException, ClassNotFoundException {
    	if(!StringUtils.isBlank(id)) {
    		MetadataTable table = metadataRes.findById(id) ;
    		if(table.isFromdb() && !StringUtils.isBlank(table.getListblocktemplet())) {
    			SysDic dic = UKeFuDic.getInstance().getDicItem(table.getListblocktemplet()) ;
    			if(dic!=null) {
	    			Object bean = UKDataContext.getContext().getBean(Class.forName(dic.getCode())) ;
	    			if(bean instanceof ElasticsearchRepository) {
	    				ElasticsearchRepository<?, ?> jpa = (ElasticsearchRepository<?, ?>)bean ;
	    				jpa.deleteAll(); 
	    			}
    			}
    		}
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table.html?id="+id+"&tabletype="+tabletype));
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked"})
	@RequestMapping("/synctoes")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView synctoes(ModelMap map , HttpServletRequest request,@Valid String id, @Valid String tabletype) throws SQLException, BeansException, ClassNotFoundException {
    	if(!StringUtils.isBlank(id)) {
    		MetadataTable table = metadataRes.findById(id) ;
    		if(table.isFromdb() && !StringUtils.isBlank(table.getListblocktemplet())) {
    			SysDic dic = UKeFuDic.getInstance().getDicItem(table.getListblocktemplet()) ;
    			
    			if(dic!=null) {
	    			Object bean = UKDataContext.getContext().getBean(Class.forName(dic.getCode())) ;
	    			if(bean instanceof ElasticsearchRepository) {
	    				ElasticsearchRepository jpa = (ElasticsearchRepository)bean ;
	    				if(!StringUtils.isBlank(table.getPreviewtemplet())) {
	    					SysDic jpaDic = UKeFuDic.getInstance().getDicItem(table.getPreviewtemplet()) ;
	    					List dataList = null ;
	    					if(table.getTablename().equals("uk_xiaoe_topic")) {
	    	    				XiaoETopiccRepository res = UKDataContext.getContext().getBean(XiaoETopiccRepository.class) ;
	    	    				dataList = res.findAll() ;
	    	    				for(Object topic : dataList) {
	    	    					Topic tp = (Topic) topic ;
	    	    					List<TopicItem> topicItemList = topicItemRes.findByTopicid(tp.getId()) ;
	    	    					for(TopicItem topicItem : topicItemList) {
	    	    						tp.getSilimar().add(topicItem.getTitle()) ;
	    	    					}
	    	    				}
	    	    			}else {
	    	    				dataList = service.list(jpaDic.getCode()) ;
	    	    			}
	    					List values = new UKeFuList();
	    					for(Object object : dataList) {
	    						values.add(object) ;
	    					}
	    					if(dataList.size() > 0) {
	    						jpa.save(values) ;
	    					}
	    				}
	    			}
    			}
    		}
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table.html?id="+id+"&tabletype="+tabletype));
    }
    
    @SuppressWarnings({ "rawtypes"})
	@RequestMapping("/synctodb")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView synctodb(ModelMap map , HttpServletRequest request,@Valid String id ,@Valid String tabletype) throws SQLException, BeansException, ClassNotFoundException {
    	if(!StringUtils.isBlank(id)) {
    		MetadataTable table = metadataRes.findById(id) ;
    		if(table.isFromdb() && !StringUtils.isBlank(table.getListblocktemplet())) {
    			SysDic dic = UKeFuDic.getInstance().getDicItem(table.getListblocktemplet()) ;
    			
    			if(dic!=null) {
	    			Object bean = UKDataContext.getContext().getBean(Class.forName(dic.getCode())) ;
	    			if(bean instanceof ElasticsearchRepository) {
	    				ElasticsearchRepository jpa = (ElasticsearchRepository)bean ;
	    				if(!StringUtils.isBlank(table.getPreviewtemplet())) {
	    					Iterable dataList = jpa.findAll();
	    					for(Object object : dataList) {
	    						service.delete(object);
	    						service.save(object);
	    					}
	    				}
	    			}
    			}
    		}
    	}
    	return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table.html?id="+id+"&tabletype="+tabletype));
    }
    
    /**
	 * 名单数据 - 跳转到编辑页面
	 * @param map
	 * @param request
	 * @param id
	 * @param nameid
	 * @return
	 */
	@RequestMapping("/estable/data/edit")
	@Menu(type = "callout" , subtype = "batch", admin = true)
	public ModelAndView dataedit(ModelMap map , HttpServletRequest request ,@Valid String tableid,@Valid String dataid,String tabletype) {
		if(!StringUtils.isBlank(tableid)) {
				MetadataTable table = metadataRes.findById(tableid);
				map.put("table", table);
				map.put("dataid", dataid);
				BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
				queryBuilder.must(termQuery("orgi", super.getOrgi(request))) ;
				queryBuilder.must(termQuery("id", dataid)) ;
				PageImpl<UKDataBean> pageImpl = esDataExchange.findPageResult(queryBuilder, UKDataContext.CALLOUT_INDEX, table, new PageRequest(super.getP(request), super.getPs(request)) , true) ;
				map.put("dataList",  pageImpl);
		}
		map.addAttribute("tabletype", tabletype);
		return request(super.createRequestPageTempletResponse("/admin/system/metadata/table/editdata"));
	}
	/**
	 * 名单数据 - 编辑保存
	 * @param map
	 * @param request
	 * @param id
	 * @param nameid
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/estable/data/editsave")
	@Menu(type = "callout" , subtype = "batch", admin = true)
	public ModelAndView dataeditsave(ModelMap map , HttpServletRequest request ,@Valid String tableid,@Valid String dataid
			, @Valid String[] fieldname, @Valid String[] value) throws Exception {
		if(!StringUtils.isBlank(tableid)) {
			MetadataTable table = metadataRes.findById(tableid);
			map.put("table", table);
			BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
			queryBuilder.must(termQuery("orgi", super.getOrgi(request))) ;
			queryBuilder.must(termQuery("id", dataid)) ;
			PageImpl<UKDataBean> findPageResult = esDataExchange.findPageResult(queryBuilder, UKDataContext.CALLOUT_INDEX, table, new PageRequest(super.getP(request), super.getPs(request)) , true) ;
			if(findPageResult != null && findPageResult.getContent().size() > 0){
				if(fieldname != null && fieldname.length > 0){
					for(int i = 0; i< fieldname.length ;i++){
						findPageResult.getContent().get(0).getValues().put(fieldname[i].toLowerCase(), value[i]);
					}
					esDataExchange.saveIObject(findPageResult.getContent().get(0));
				}
			}
		}
		return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table/data.html?id="+tableid));
	}
	/**
	 * 名单数据 - 删除
	 * @param map
	 * @param request
	 * @param id
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/estable/data/delete")
	@Menu(type = "callout" , subtype = "batch", admin = true)
	public ModelAndView datadelete(ModelMap map , HttpServletRequest request ,@Valid String tableid,@Valid String dataid) throws Exception {
		if(!StringUtils.isBlank(tableid)) {
			MetadataTable table = metadataRes.findById(tableid);
			map.put("table", table);
			BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
			queryBuilder.must(termQuery("orgi", super.getOrgi(request))) ;
			queryBuilder.must(termQuery("id", dataid)) ;
			PageImpl<UKDataBean> findPageResult = esDataExchange.findPageResult(queryBuilder, UKDataContext.CALLOUT_INDEX, table, new PageRequest(super.getP(request), super.getPs(request)) , true) ;
			esDataExchange.deleteIObject(findPageResult.getContent().get(0));
		}
		return request(super.createRequestPageTempletResponse("redirect:/admin/metadata/table/data.html?id="+tableid));
	}
}