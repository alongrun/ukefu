package com.ukefu.webim.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.ukefu.util.UKTools;

@Entity
@Table(name = "uk_columnproperties")
@org.hibernate.annotations.Proxy(lazy = false)
public class ColumnProperties implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id = UKTools.getUUID();
	private String modelid;
	private String dataid ;
	private String dataname ; 	//如果存放的是指标，则此处为指标名称
	private String title ;
	private String colname;//列标题
	private String border;//边框
	private String width;//列宽度
	private String format;//列数据格式化
	private String decimalcount;//小数位数
	private String sepsymbol;//分隔符
	private String font;///文字大小
	private String alignment;//对齐方式
	private String fontstyle;//字体样式
	private String fontcolor;//字体颜色
	private String cur;//货币
	private String timeformat;//日期时间
	private String hyp;//超链接
	private String prefix;//前缀
	private String suffix;//后缀
	private String paramname ;//参数名
	private String orgi;
	private int sortindex=1;
	private String value ;	//控制替换
	
	private int fixed ;
	private boolean sort ;
	private String type ;
	private String minwidth ;
	private boolean unresize ;//是否禁用拖拽列宽
	
	private String dbtableid ;
	
	private String datatypename ;
	
	private boolean seldata ; //是否启用字典
	private String seldatacode ;//字典项
	
	private String placeholder ;//占位符
	private boolean required ; //是否必填
	private boolean readonly ; //是否只读
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getModelid() {
		return modelid;
	}
	public void setModelid(String modelid) {
		this.modelid = modelid;
	}
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public String getDataname() {
		return dataname;
	}
	public void setDataname(String dataname) {
		this.dataname = dataname;
	}
	public String getColname() {
		return colname;
	}
	public void setColname(String colname) {
		this.colname = colname;
	}
	public String getBorder() {
		return border;
	}
	public void setBorder(String border) {
		this.border = border;
	}
	public String getWidth() {
		return width;
	}
	public void setWidth(String width) {
		this.width = width;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getdecimalcount() {
		return decimalcount;
	}
	public void setdecimalcount(String decimalcount) {
		this.decimalcount = decimalcount;
	}
	
	public String getSepsymbol() {
		return sepsymbol;
	}
	public void setSepsymbol(String sepsymbol) {
		this.sepsymbol = sepsymbol;
	}
	public String getFont() {
		return font;
	}
	public void setFont(String font) {
		this.font = font;
	}
	public String getAlignment() {
		return alignment;
	}
	public void setAlignment(String alignment) {
		this.alignment = alignment;
	}
	public String getfontstyle() {
		return fontstyle;
	}
	public void setfontstyle(String fontstyle) {
		this.fontstyle = fontstyle;
	}
	public String getfontcolor() {
		return fontcolor;
	}
	public void setfontcolor(String fontcolor) {
		this.fontcolor = fontcolor;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getparamname() {
		return paramname;
	}
	public void setparamname(String paramname) {
		this.paramname = paramname;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCur() {
		return cur;
	}
	public void setCur(String cur) {
		this.cur = cur;
	}
	public String gettimeformat() {
		return timeformat;
	}
	public void settimeformat(String timeformat) {
		this.timeformat = timeformat;
	}
	public String getHyp() {
		return hyp;
	}
	public void setHyp(String hyp) {
		this.hyp = hyp;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getSortindex() {
		return sortindex;
	}
	public void setSortindex(int sortindex) {
		this.sortindex = sortindex;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public int getFixed() {
		return fixed;
	}
	public void setFixed(int fixed) {
		this.fixed = fixed;
	}
	public boolean isSort() {
		return sort;
	}
	public void setSort(boolean sort) {
		this.sort = sort;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMinwidth() {
		return minwidth;
	}
	public void setMinwidth(String minwidth) {
		this.minwidth = minwidth;
	}
	public String getDbtableid() {
		return dbtableid;
	}
	public void setDbtableid(String dbtableid) {
		this.dbtableid = dbtableid;
	}
	public String getDatatypename() {
		return datatypename;
	}
	public void setDatatypename(String datatypename) {
		this.datatypename = datatypename;
	}
	public boolean isUnresize() {
		return unresize;
	}
	public void setUnresize(boolean unresize) {
		this.unresize = unresize;
	}
	public boolean isSeldata() {
		return seldata;
	}
	public void setSeldata(boolean seldata) {
		this.seldata = seldata;
	}
	public String getSeldatacode() {
		return seldatacode;
	}
	public void setSeldatacode(String seldatacode) {
		this.seldatacode = seldatacode;
	}
	public String getPlaceholder() {
		return placeholder;
	}
	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}
	public boolean isRequired() {
		return required;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public boolean isReadonly() {
		return readonly;
	}
	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}
	
}
