package com.ukefu.webim.service.task;

import java.io.File;

public class RuntimeData {

	private String ipaddr = null;
	private String hostname = null;
	
	public long getDiskSpace() {
		return new File(".").getTotalSpace();
	}
	public void setDiskSpace(long diskSpace) {
	}
	public long getDiskFreeSpace() {
		return new File(".").getFreeSpace();
	}
	public long getUsedDiskSpace() {
		return this.getDiskSpace() - this.getDiskFreeSpace();
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
}
