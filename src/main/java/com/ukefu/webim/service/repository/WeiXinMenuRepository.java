package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.WeiXinMenu;
import com.ukefu.webim.web.model.WeiXinUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface WeiXinMenuRepository extends JpaRepository<WeiXinMenu, String>{

	public abstract WeiXinMenu findByIdAndOrgi(String id, String orgi);

	public abstract List<WeiXinMenu> findBySnsidAndOrgiAndParentidOrderBySortindexAsc(String snsid, String orgi,String parentId);

	public abstract List<WeiXinMenu> findBySnsidAndOrgiOrderBySortindexAsc(String snsid, String orgi);

}
