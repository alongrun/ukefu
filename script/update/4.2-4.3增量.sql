CREATE TABLE `uk_crm_datamodel` (
  `ID` varchar(32) NOT NULL DEFAULT '' COMMENT 'ID',
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `REPORTTYPE` varchar(32) DEFAULT NULL COMMENT '类型',
  `TITLE` varchar(255) DEFAULT NULL COMMENT '标题',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `OBJECTCOUNT` int(11) DEFAULT NULL COMMENT '对象数量',
  `DICID` varchar(32) DEFAULT NULL COMMENT '菜单ID',
  `CREATETIME` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `DESCRIPTION` longtext COMMENT '描述',
  `HTML` longtext COMMENT '代码',
  `REPORTPACKAGE` varchar(255) DEFAULT NULL COMMENT '目录名称',
  `USEACL` varchar(32) DEFAULT NULL COMMENT '授权信息',
  `status` varchar(32) DEFAULT NULL COMMENT '状态',
  `rolename` text COMMENT '角色名称',
  `userid` text COMMENT '创建用户ID',
  `blacklist` text COMMENT '黑名单',
  `REPORTCONTENT` longtext COMMENT '对象内容',
  `reportmodel` varchar(32) DEFAULT NULL COMMENT '模型',
  `updatetime` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  `creater` varchar(255) DEFAULT NULL COMMENT '创建人',
  `reportversion` int(11) DEFAULT NULL COMMENT '版本号',
  `publishedtype` varchar(32) DEFAULT NULL COMMENT '发布状态',
  `tabtype` varchar(32) DEFAULT NULL COMMENT '元素类型',
  `USERNAME` varchar(32) DEFAULT NULL COMMENT '用户名',
  `USEREMAIL` varchar(255) DEFAULT NULL COMMENT '用户邮件信息',
  `CACHE` smallint(6) DEFAULT NULL COMMENT '是否启用数据缓存',
  `EXTPARAM` varchar(255) DEFAULT NULL COMMENT '扩展数据',
  `TARGETREPORT` varchar(32) DEFAULT NULL COMMENT '模板对象',
  `VIEWTYPE` varchar(32) DEFAULT NULL COMMENT '对象类型',
  `ACTIONTYPE` varchar(32) DEFAULT NULL COMMENT '操作类型',
  `STYLE` varchar(32) DEFAULT NULL COMMENT '样式信息',
  `ICON` varchar(32) DEFAULT NULL COMMENT '图标',
  `TBID` varchar(32) DEFAULT NULL COMMENT '元数据ID',
  `TBNAME` varchar(255) DEFAULT NULL COMMENT '元数据名称',
  `ACTION` varchar(32) DEFAULT NULL COMMENT '动作',
  `BTNTYPE` varchar(32) DEFAULT NULL COMMENT '按钮类型',
  `DESIGN` varchar(32) DEFAULT NULL COMMENT '显示值',
  `SORTINDEX` int(11) DEFAULT NULL COMMENT '排序序号',
  `PROID` varchar(32) DEFAULT NULL COMMENT '产品ID',
  `AUTHCODE` varchar(50) DEFAULT NULL COMMENT '权限代码',
  `AUTHITEM` varchar(50) DEFAULT NULL COMMENT '授权时间',
  `DSTYPE` varchar(32) DEFAULT NULL COMMENT '数据源类型',
  `RTFEDIT` tinyint(4) DEFAULT NULL COMMENT '是否允许编辑文本',
  `CODEEDIT` tinyint(4) DEFAULT NULL COMMENT '是否允许编辑代码',
  `LINKTYPE` varchar(32) DEFAULT NULL COMMENT '链接类型',
  `LINKURL` varchar(255) DEFAULT NULL COMMENT '链接跳转URL',
  `PARAMS` varchar(255) DEFAULT NULL COMMENT '扩展参数',
  `LAYOUTTYPE` varchar(32) DEFAULT NULL COMMENT '布局类型',
  `upload` tinyint(4) DEFAULT NULL COMMENT '是否启用上传',
  `hasform` tinyint(4) DEFAULT NULL COMMENT '是否有表单',
  `formtype` varchar(32) DEFAULT NULL COMMENT '表单类型',
  `submiturl` varchar(255) DEFAULT NULL COMMENT '提交跳转的URL',
  `submitpage` varchar(255) DEFAULT NULL COMMENT '跳转页面',
  `submitlink` varchar(255) DEFAULT NULL COMMENT '提交URL',
  `reseturl` varchar(255) DEFAULT NULL COMMENT '重置后跳转URL',
  `resetpage` varchar(255) DEFAULT NULL COMMENT '重置后跳转地址',
  `resetlink` varchar(255) DEFAULT NULL COMMENT '重置链接',
  `resetbtn` tinyint(4) DEFAULT NULL COMMENT '是否启用重置按钮',
  `submitbtn` tinyint(4) DEFAULT NULL COMMENT '是否启用提交按钮',
  `resetpagerpt` varchar(32) DEFAULT NULL COMMENT '重置页面',
  `submitpagerpt` varchar(255) DEFAULT NULL COMMENT '提交页面',
  `layoutleft` tinyint(4) DEFAULT NULL COMMENT '启用左侧分栏',
  `layoutright` tinyint(4) DEFAULT NULL COMMENT '启用右侧分栏',
  `layoutcenter` tinyint(4) DEFAULT NULL COMMENT '启用中间分栏',
  `leftscroll` tinyint(4) DEFAULT NULL COMMENT '启用左侧滚动',
  `rightscroll` tinyint(4) DEFAULT NULL COMMENT '启用右侧滚动',
  `centerscroll` tinyint(4) DEFAULT NULL COMMENT '启用中间滚动',
  `leftwidth` varchar(32) DEFAULT NULL COMMENT '左侧间隔',
  `rightwidth` varchar(32) DEFAULT NULL COMMENT '右侧间隔',
  `centerheight` varchar(32) DEFAULT NULL COMMENT '中间间隔',
  `mediaagent` tinyint(4) DEFAULT NULL COMMENT '坐席',
  `hisnav` tinyint(4) DEFAULT NULL COMMENT '是否有导航',
  `submittype` varchar(32) DEFAULT NULL COMMENT '提交类型',
  `submitpos` varchar(32) DEFAULT NULL COMMENT '表单提交后更新页面位置',
  `MGROUP` tinyint(4) DEFAULT '0' COMMENT '是否启用分组',
  `groupname` varchar(100) DEFAULT NULL COMMENT '分组名称',
  `groupicon` varchar(100) DEFAULT NULL COMMENT '分组图标',
  `groupcolor` varchar(100) DEFAULT NULL COMMENT '分组颜色',
  `workflow` tinyint(4) DEFAULT NULL COMMENT '启用流程',
  `flowtype` varchar(50) DEFAULT NULL COMMENT '流程类型',
  `successtip` varchar(255) DEFAULT NULL COMMENT '成功提示',
  `errortip` varchar(255) DEFAULT NULL COMMENT '失败提示',
  `fullscreen` tinyint(4) DEFAULT NULL COMMENT '全屏',
  `autorefresh` tinyint(4) DEFAULT NULL COMMENT '自动刷新',
  `refreshtime` int(11) DEFAULT NULL COMMENT '刷新间隔',
  `autoscroll` tinyint(4) DEFAULT NULL COMMENT '自动滚动',
  `scrollspeed` varchar(32) DEFAULT NULL COMMENT '滚动速度',
  `onlytab` tinyint(4) DEFAULT NULL COMMENT '大屏显示的时候是否只显示 页签',
  `rotationspeed` int(11) DEFAULT NULL COMMENT '大屏显示的时候轮播速度',
  `accesshis` tinyint(4) DEFAULT NULL COMMENT '是否记录 访问历史',
  `searchhis` tinyint(4) DEFAULT NULL COMMENT '是否记录搜索历史',
  `CODE` varchar(100) DEFAULT NULL COMMENT '代码',
  `duplicate` tinyint(4) DEFAULT NULL COMMENT '是否允许重复数据',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `uk_crm_dataproduct` (
  `ID` varchar(32) NOT NULL COMMENT 'ID',
  `NAME` varchar(32) DEFAULT NULL COMMENT '名称',
  `TITLE` varchar(32) DEFAULT NULL COMMENT '标题',
  `CODE` varchar(32) DEFAULT NULL COMMENT ' 代码',
  `PARENTID` varchar(32) DEFAULT NULL COMMENT '上级ID',
  `TYPE` varchar(32) DEFAULT NULL COMMENT '类型',
  `MEMO` varchar(255) DEFAULT NULL COMMENT '备注',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `STATUS` varchar(32) DEFAULT NULL COMMENT '状态',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `CREATER` varchar(255) DEFAULT NULL COMMENT '创建人',
  `PUBLISHEDTYPE` varchar(32) DEFAULT NULL COMMENT '发布类型',
  `DESCRIPTION` varchar(255) DEFAULT NULL COMMENT '描述信息',
  `TABTYPE` varchar(32) DEFAULT NULL COMMENT '产品类型',
  `DSTYPE` varchar(32) DEFAULT NULL COMMENT '显示类型',
  `DSTEMPLET` varchar(255) DEFAULT NULL COMMENT '显示模板',
  `SORTINDEX` int(11) DEFAULT NULL COMMENT '排序序号',
  `DICTYPE` varchar(32) DEFAULT NULL COMMENT '目录类型',
  `ICONCLASS` varchar(100) DEFAULT NULL COMMENT '图标样式',
  `CSSSTYLE` varchar(255) DEFAULT NULL COMMENT '显示样式',
  `AUTHCODE` varchar(100) DEFAULT NULL COMMENT '授权代码',
  `DEFAULTMENU` tinyint(4) DEFAULT NULL COMMENT '默认产品',
  `DATAID` varchar(32) DEFAULT NULL COMMENT '外部数据ID',
  `DICICON` varchar(32) DEFAULT NULL COMMENT ' 目录图标',
  `CURICON` varchar(32) DEFAULT NULL COMMENT '选中图标',
  `BGCOLOR` varchar(32) DEFAULT NULL COMMENT '背景颜色',
  `CURBGCOLOR` varchar(32) DEFAULT NULL COMMENT '选中的背景颜色',
  `MENUPOS` varchar(32) DEFAULT NULL COMMENT '菜单位置',
  `DISTITLE` varchar(100) DEFAULT NULL COMMENT '显示标题',
  `NAVMENU` tinyint(4) DEFAULT '0' COMMENT '显示菜单',
  `QUICKMENU` tinyint(4) DEFAULT '0' COMMENT '是否显示在快捷菜单',
  UNIQUE KEY `SQL121227155530400` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE uk_noticemsg ADD invalidtime datetime DEFAULT NULL COMMENT '失效时间';

ALTER TABLE uk_consult_invite ADD lvtipmsg text DEFAULT NULL COMMENT '无坐席在线的提示消息';
ALTER TABLE uk_crm_dataproduct ADD duplicate tinyint(4) DEFAULT 0 COMMENT '是否允许重复数据';
ALTER TABLE uk_crm_dataproduct ADD design tinyint(4) DEFAULT 0 COMMENT '布局';

DROP INDEX index_1 ON uk_callcenter_event;
CREATE INDEX index_1 ON uk_callcenter_event  (`ORGI`,`DISCALLER`,`DISCALLED`,`MISSCALL`,`DURATION`,`RINGDURATION`,`calltype`,`servicestatus`,`direction`,`userid`,`organ`,`createtime`,`starttime`,`nameid`) USING BTREE;




ALTER TABLE uk_consult_invite ADD filterscript tinyint(4) DEFAULT 0 COMMENT '禁止访客端发送HTML内容';
ALTER TABLE uk_consult_invite ADD filteragentscript tinyint(4) DEFAULT 0 COMMENT '禁止坐席端发送HTML内容';

ALTER TABLE uk_consult_invite ADD filteragentscript tinyint(4) DEFAULT 0 COMMENT '禁止坐席端发送HTML内容';

ALTER TABLE uk_consult_invite ADD filteragentscript tinyint(4) DEFAULT 0 COMMENT '禁止坐席端发送HTML内容';

ALTER TABLE uk_agentuser ADD filterscript int DEFAULT 0 COMMENT '访客段脚本过滤次数';
ALTER TABLE uk_agentuser ADD filteragentscript int DEFAULT 0 COMMENT '座席端脚本过滤次数';

ALTER TABLE uk_agentuser ADD sensitiveword int DEFAULT 0 COMMENT '访客端敏感词触发次数';
ALTER TABLE uk_agentuser ADD sensitivewordagent int DEFAULT 0 COMMENT '坐席端敏感词触发次数';

ALTER TABLE uk_agentuser ADD msgtimeout int DEFAULT 0 COMMENT '访客端消息超时次数';
ALTER TABLE uk_agentuser ADD msgtimeoutagent int DEFAULT 0 COMMENT '坐席端消息敏感词触发次数';

ALTER TABLE uk_agentuser ADD sessiontimeout int DEFAULT 0 COMMENT '会话超时次数';


ALTER TABLE uk_agentservice ADD filterscript int DEFAULT 0 COMMENT '访客段脚本过滤次数';
ALTER TABLE uk_agentservice ADD filteragentscript int DEFAULT 0 COMMENT '座席端脚本过滤次数';

ALTER TABLE uk_agentservice ADD sensitiveword int DEFAULT 0 COMMENT '访客端敏感词触发次数';
ALTER TABLE uk_agentservice ADD sensitivewordagent int DEFAULT 0 COMMENT '坐席端敏感词触发次数';

ALTER TABLE uk_agentservice ADD msgtimeout int DEFAULT 0 COMMENT '访客端消息超时次数';
ALTER TABLE uk_agentservice ADD msgtimeoutagent int DEFAULT 0 COMMENT '坐席端消息敏感词触发次数';

ALTER TABLE uk_agentservice ADD sessiontimeout int DEFAULT 0 COMMENT '会话超时次数';


ALTER TABLE uk_chat_message ADD filterscript tinyint(4) DEFAULT 0 COMMENT '触发了HTML代码过滤';


ALTER TABLE uk_callcenter_extention ADD greetlong VARCHAR(100) DEFAULT NULL COMMENT '欢迎提示语音';
ALTER TABLE uk_callcenter_extention ADD greetshort VARCHAR(100) DEFAULT NULL COMMENT '欢迎提示短语音';
ALTER TABLE uk_callcenter_extention ADD invalidsound VARCHAR(100) DEFAULT NULL COMMENT '无效输入提示语音';
ALTER TABLE uk_callcenter_extention ADD exitsound VARCHAR(100) DEFAULT NULL COMMENT '离开语音';
ALTER TABLE uk_callcenter_extention ADD confirmmacro VARCHAR(50) DEFAULT NULL COMMENT '确认宏指令';
ALTER TABLE uk_callcenter_extention ADD confirmkey VARCHAR(50) DEFAULT NULL COMMENT '确认按键';
ALTER TABLE uk_callcenter_extention ADD ttsengine VARCHAR(20) DEFAULT NULL COMMENT 'TTS引擎';
ALTER TABLE uk_callcenter_extention ADD ttsvoice VARCHAR(50) DEFAULT NULL COMMENT 'TTS语音';
ALTER TABLE uk_callcenter_extention ADD confirmattempts VARCHAR(50) DEFAULT NULL COMMENT '确认提示消息';
ALTER TABLE uk_callcenter_extention ADD timeout int DEFAULT 0 COMMENT '超时时间';

ALTER TABLE uk_callcenter_extention ADD interdigittimeout int DEFAULT 0 COMMENT '呼叫等待超时';
ALTER TABLE uk_callcenter_extention ADD maxfailures int DEFAULT 0 COMMENT '最大失败次数';

ALTER TABLE uk_callcenter_extention ADD maxtimeouts int DEFAULT 0 COMMENT '最大超时次数';

ALTER TABLE uk_callcenter_extention ADD digitlen int DEFAULT 0 COMMENT '数字按键长度';
ALTER TABLE uk_callcenter_extention ADD action VARCHAR(50) DEFAULT NULL COMMENT '指令';
ALTER TABLE uk_callcenter_extention ADD digits VARCHAR(50) DEFAULT NULL COMMENT '拨号键';
ALTER TABLE uk_callcenter_extention ADD param VARCHAR(255) DEFAULT NULL COMMENT '参数';


ALTER TABLE uk_consult_invite ADD enableinvite int DEFAULT 0 COMMENT '显示默认咨询快捷提示';
ALTER TABLE uk_consult_invite ADD invitetiptitle varchar(50) DEFAULT NULL COMMENT '邀请咨询组件提示标题' ;
ALTER TABLE uk_consult_invite ADD invitetip text DEFAULT NULL COMMENT '邀请咨询提示HTML' ;
ALTER TABLE uk_consult_invite ADD invitetipdelay int DEFAULT 0 COMMENT '延时显示' ;

ALTER TABLE uk_consult_invite ADD enablecallback int DEFAULT 0 COMMENT '显示回呼组件';
ALTER TABLE uk_consult_invite ADD callbacknum  VARCHAR(50) DEFAULT NULL COMMENT '回呼号码' ;	
ALTER TABLE uk_consult_invite ADD callbacktxt  VARCHAR(50) DEFAULT NULL COMMENT '回呼提示文字' ;	
ALTER TABLE uk_consult_invite ADD callbackquicktiptitle varchar(50) DEFAULT NULL COMMENT '回呼提示标题' ;
ALTER TABLE uk_consult_invite ADD callbackquicktip text DEFAULT NULL COMMENT '回呼提示HTML' ;
ALTER TABLE uk_consult_invite ADD callbackurl  VARCHAR(255) DEFAULT NULL COMMENT '回呼点击后跳转' ;
ALTER TABLE uk_consult_invite ADD callbackicon  VARCHAR(50) DEFAULT NULL COMMENT '回呼图标' ;	
ALTER TABLE uk_consult_invite ADD callbackcolor  VARCHAR(50) DEFAULT NULL COMMENT '回呼背景颜色' ;	

ALTER TABLE uk_consult_invite ADD enabledemo int DEFAULT 0 COMMENT '显示预约演示组件';
ALTER TABLE uk_consult_invite ADD demourl  VARCHAR(255) DEFAULT NULL COMMENT '预约演示URL' ;	
ALTER TABLE uk_consult_invite ADD demotxt  VARCHAR(50) DEFAULT NULL COMMENT '预约演示文字' ;	
ALTER TABLE uk_consult_invite ADD demoquicktiptitle varchar(50) DEFAULT NULL COMMENT '预约演示提示标题' ; 	
ALTER TABLE uk_consult_invite ADD demoquicktip text DEFAULT NULL COMMENT '预约演示提示HTML' ; 	
ALTER TABLE uk_consult_invite ADD demoicon  VARCHAR(50) DEFAULT NULL COMMENT '预约演示图标' ;	
ALTER TABLE uk_consult_invite ADD democolor  VARCHAR(50) DEFAULT NULL COMMENT '预约演示背景颜色' ;	


ALTER TABLE uk_consult_invite ADD enablesns int DEFAULT 0 COMMENT '显示微信公众号组件';
ALTER TABLE uk_consult_invite ADD snsurl  VARCHAR(255) DEFAULT NULL COMMENT '公众号跳转URL' ;		
ALTER TABLE uk_consult_invite ADD snstxt  VARCHAR(50) DEFAULT NULL COMMENT '公众号提示文字' ;		
ALTER TABLE uk_consult_invite ADD snstiptitle varchar(50) DEFAULT NULL COMMENT '公众号提示标题' ; 	
ALTER TABLE uk_consult_invite ADD snstip text DEFAULT NULL COMMENT '公众号提示HTML' ; 	
ALTER TABLE uk_consult_invite ADD snsicon  VARCHAR(50) DEFAULT NULL COMMENT '公众号图标' ;	
ALTER TABLE uk_consult_invite ADD snsqrcode  VARCHAR(50) DEFAULT NULL COMMENT '公众号二维码' ;	
ALTER TABLE uk_consult_invite ADD snscolor  VARCHAR(50) DEFAULT NULL COMMENT '公众号背景颜色' ;	


ALTER TABLE uk_consult_invite ADD enableothermodel int DEFAULT 0 COMMENT '显示其他组件';
ALTER TABLE uk_consult_invite ADD othermodelurl  VARCHAR(255) DEFAULT NULL COMMENT '其他组件URL' ;	
ALTER TABLE uk_consult_invite ADD othermodeltxt  VARCHAR(50) DEFAULT NULL COMMENT '其他组件文本' ;	
ALTER TABLE uk_consult_invite ADD othermodeltiptitle varchar(50) DEFAULT NULL COMMENT '其他组件提示标题' ;	
ALTER TABLE uk_consult_invite ADD othermodeltip text DEFAULT NULL COMMENT '其他组件提示HTML' ;	
ALTER TABLE uk_consult_invite ADD othermodelicon  VARCHAR(50) DEFAULT NULL COMMENT '其他组件图标' ;	
ALTER TABLE uk_consult_invite ADD othermodelcolor  VARCHAR(50) DEFAULT NULL COMMENT '其他组件背景颜色' ;

ALTER TABLE uk_crm_datamodel ADD targetid varchar(32) DEFAULT NULL COMMENT '目标id';
ALTER TABLE uk_crm_dataproduct ADD TBID varchar(32) DEFAULT NULL COMMENT '元数据ID';
ALTER TABLE uk_crm_dataproduct ADD menuid varchar(32) DEFAULT NULL COMMENT '菜单ID';
ALTER TABLE uk_crm_dataproduct ADD scheme varchar(255) DEFAULT NULL COMMENT '方案';

INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('40281381692dde0901692de349610012', '链接', 'pub', 'com.dic.href', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-2-27 15:37:44', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `uk_sysdic` (`ID`, `NAME`, `TITLE`, `CODE`, `ORGI`, `CTYPE`, `PARENTID`, `DESCRIPTION`, `MEMO`, `ICONSTR`, `ICONSKIN`, `CATETYPE`, `CREATER`, `CREATETIME`, `UPDATETIME`, `HASCHILD`, `SORTINDEX`, `DICID`, `DEFAULTVALUE`, `DISCODE`, `URL`, `MODULE`, `MLEVEL`, `RULES`, `MENUTYPE`) VALUES ('40281381692dde0901692de2f45b000b', '按钮', 'pub', 'com.dic.button', NULL, 'data', '0', '', NULL, NULL, NULL, NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-2-27 15:37:22', NULL, 1, 0, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL);

ALTER TABLE uk_tableproperties ADD freesearch tinyint(4) DEFAULT 0 COMMENT '搜索方式（0:不支持，1：快速搜索，2：高级搜索）';

ALTER TABLE uk_crm_datamodel ADD targetid varchar(32) DEFAULT NULL COMMENT '目标id';
ALTER TABLE uk_crm_dataproduct ADD TBID varchar(32) DEFAULT NULL COMMENT '元数据ID';
ALTER TABLE uk_crm_dataproduct ADD menuid varchar(32) DEFAULT NULL COMMENT '菜单ID';
ALTER TABLE uk_crm_dataproduct ADD scheme varchar(255) DEFAULT NULL COMMENT '方案';

CREATE TABLE `uk_crm_scheme` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `code` varchar(255) DEFAULT NULL COMMENT '代码',
  `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
  `createtime` date DEFAULT NULL COMMENT '创建时间',
  `updatetime` date DEFAULT NULL COMMENT '更新时间',
  `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='crm-方案表';

ALTER TABLE uk_crm_dataproduct ADD datamodelid varchar(32) DEFAULT NULL COMMENT '默认页面布局id';
ALTER TABLE uk_crm_dataproduct ADD skiptype varchar(32) DEFAULT NULL COMMENT '跳转方式';
ALTER TABLE uk_crm_dataproduct ADD skipurl varchar(32) DEFAULT NULL COMMENT '跳转url';
ALTER TABLE uk_crm_dataproduct ADD defaulshow tinyint(4) DEFAULT '0' COMMENT '是否默认显示';
ALTER TABLE uk_crm_dataproduct ADD iconcolor varchar(100) DEFAULT NULL COMMENT '图标颜色';

ALTER TABLE uk_crm_dataproduct ADD allowfilter tinyint(4) DEFAULT '0' COMMENT '是否允许筛选列';
ALTER TABLE uk_crm_dataproduct ADD allowprint tinyint(4) DEFAULT '0' COMMENT '是否允许打印';
ALTER TABLE uk_crm_dataproduct ADD allowexports tinyint(4) DEFAULT '0' COMMENT '是否允许导出';

ALTER TABLE uk_callcenter_ivr ADD playsound VARCHAR(100) DEFAULT NULL COMMENT '播放语音';

ALTER TABLE uk_agentservice ADD queuetime datetime DEFAULT NULL COMMENT '进入队列时间';
ALTER TABLE uk_agentuser ADD queuetime datetime DEFAULT NULL COMMENT '进入队列时间';


ALTER TABLE uk_callcenter_extention ADD autoanswer varchar(20) DEFAULT NULL COMMENT '来电自动接听';
ALTER TABLE uk_agentuser ADD queuetime datetime DEFAULT NULL COMMENT '进入队列时间';
ALTER TABLE uk_agentuser ADD queuetime datetime DEFAULT NULL COMMENT '进入队列时间';

ALTER TABLE uk_que_result ADD answerlevel VARCHAR(50) DEFAULT NULL COMMENT '评级 a b c d';

ALTER TABLE uk_crm_datamodel ADD parentid varchar(32) DEFAULT NULL COMMENT '父级id';


ALTER TABLE uk_callcenter_event ADD autoanswer tinyint DEFAULT 0 COMMENT '是否自动接听';

ALTER TABLE uk_agentuser ADD endby varchar(20) DEFAULT NULL COMMENT '挂断方';
ALTER TABLE uk_callcenter_event ADD autoanswer tinyint DEFAULT 0 COMMENT '是否自动接听';


ALTER TABLE uk_noticemsg ADD sqlurl varchar(255) DEFAULT NULL COMMENT '下载sql地址';
ALTER TABLE uk_noticemsg ADD version varchar(255) DEFAULT NULL COMMENT '版本号';
ALTER TABLE uk_systemconfig ADD version varchar(255) DEFAULT NULL COMMENT '版本号';

ALTER TABLE uk_noticemsg ADD jarurldownload TINYINT(4) DEFAULT 0 COMMENT 'jar包是否下载成功';
ALTER TABLE uk_noticemsg ADD sqlurldownload TINYINT(4) DEFAULT 0 COMMENT 'sql是否下载成功';
ALTER TABLE uk_noticemsg ADD udpatestatus TINYINT(4) DEFAULT 0 COMMENT '更新状态 0未更新 1已执行更新';

ALTER TABLE uk_noticemsg ADD rollbacksqlurl varchar(255) DEFAULT NULL COMMENT '下载回滚sql地址';

ALTER TABLE uk_noticemsg ADD rollbacksqlurldownload TINYINT(4) DEFAULT 0 COMMENT '回滚sql是否下载成功';

ALTER TABLE uk_system_updatecon ADD confirm TINYINT(4) DEFAULT NULL COMMENT '是否确认更新';
ALTER TABLE uk_system_updatecon ADD jarurl varchar(255) DEFAULT NULL COMMENT '下载sql地址';
ALTER TABLE uk_system_updatecon ADD sqlurl varchar(255) DEFAULT NULL COMMENT '下载sql地址';
ALTER TABLE uk_system_updatecon ADD oldversion varchar(255) DEFAULT NULL COMMENT '版本号';
ALTER TABLE uk_system_updatecon ADD version varchar(255) DEFAULT NULL COMMENT '版本号';
ALTER TABLE uk_system_updatecon ADD versiondesc varchar(500) DEFAULT NULL COMMENT '版本更新说明';
ALTER TABLE uk_system_updatecon ADD jarurldownload TINYINT(4) DEFAULT 0 COMMENT 'jar包是否下载成功';
ALTER TABLE uk_system_updatecon ADD sqlurldownload TINYINT(4) DEFAULT 0 COMMENT 'sql是否下载成功';
ALTER TABLE uk_system_updatecon ADD udpatestatus TINYINT(4) DEFAULT 0 COMMENT '更新状态 0未更新 1已执行更新';

ALTER TABLE uk_system_updatecon ADD rollbacksqlurl varchar(255) DEFAULT NULL COMMENT '下载回滚sql地址';

ALTER TABLE uk_system_updatecon ADD rollbacksqlurldownload TINYINT(4) DEFAULT 0 COMMENT '回滚sql是否下载成功';

ALTER TABLE uk_systemconfig ADD appid varchar(255) DEFAULT NULL COMMENT '客户端id';



ALTER TABLE uk_callcenter_event ADD answersip text DEFAULT NULL COMMENT '应答事件SIP';
ALTER TABLE uk_callcenter_event ADD hangupsip text DEFAULT NULL COMMENT '挂断事件SIP';

ALTER TABLE uk_agentservice ADD useful tinyint(4) DEFAULT '0' COMMENT '是否解决（1 解决），该会话是否解决了访客问题，一般用在机器人会话';

ALTER TABLE uk_chat_message ADD puremsg text DEFAULT NULL COMMENT '纯文本消息';

ALTER TABLE uk_xiaoe_config ADD sysmsgtransagent tinyint(4) DEFAULT 0 COMMENT '未命中转人工按钮';

ALTER TABLE uk_xiaoe_config ADD sysmsgtransagentmsg varchar(255) DEFAULT NULL COMMENT '未命中转人工提示文本';

CREATE TABLE `uk_sensitivewords` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `KEYWORD` varchar(50) DEFAULT NULL COMMENT '敏感词',
  `CONTENT` text COMMENT '内容',
  `CREATETIME` datetime DEFAULT NULL COMMENT '创建时间',
  `CREATER` varchar(32) DEFAULT NULL COMMENT '创建人',
  `UPDATETIME` datetime DEFAULT NULL COMMENT '更新时间',
  `ORGI` varchar(32) DEFAULT NULL COMMENT '租户ID',
  `USERNAME` varchar(50) DEFAULT NULL COMMENT '用户名',
  `SUPERORDINATE` varchar(50) DEFAULT NULL COMMENT '上位词',
  `PARTOFSPEECH` varchar(50) DEFAULT NULL COMMENT '词性',
  `CATE` varchar(32) DEFAULT NULL COMMENT '分类',
  `type` varchar(32) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='敏感词';

ALTER TABLE uk_sessionconfig ADD responsetimeout TINYINT(4) DEFAULT 0 COMMENT '是否监控响应超时';
ALTER TABLE uk_sessionconfig ADD responsetimeouts INT(11) DEFAULT 0 COMMENT '监控响应超时时长';
ALTER TABLE uk_sessionconfig ADD sestimeout TINYINT(4) DEFAULT 0 COMMENT '是否监控会话超时';
ALTER TABLE uk_sessionconfig ADD sestimeouts INT(11) DEFAULT 0 COMMENT '监控会话超时时长';
ALTER TABLE uk_sessionconfig ADD inquenetimeout TINYINT(4) DEFAULT 0 COMMENT '是否监控排队会话超时';
ALTER TABLE uk_sessionconfig ADD inquenetimeouts INT(11) DEFAULT 0 COMMENT '监控会话排队时长';
ALTER TABLE uk_sessionconfig ADD agentinvalid TINYINT(4) DEFAULT 0 COMMENT '是否监控会话无效';
ALTER TABLE uk_sessionconfig ADD satisfactions TINYINT(4) DEFAULT 0 COMMENT '是否监控会话满意度';
ALTER TABLE uk_sessionconfig ADD satislevels varchar(50) DEFAULT NULL COMMENT '监控满意度级别';



CREATE INDEX index_2 ON uk_chat_message  (`ORGI`,`chatype`,`agentserviceid`,`createtime`,`updatetime`) USING BTREE;
CREATE INDEX index_1 ON uk_onlineuser  (`STATUS`,`orgi`,`createtime`) USING BTREE;

ALTER TABLE uk_agentuser ADD satisfactionalarms INT(11) DEFAULT 0 COMMENT '满意度报警次数';
ALTER TABLE uk_agentuser ADD invitevals INT(11) DEFAULT 0 COMMENT '邀请评价次数';
ALTER TABLE uk_agentuser ADD resptimeouts INT(11) DEFAULT 0 COMMENT '响应超时次数';

DROP TABLE IF EXISTS `wf_cc_order`;
CREATE TABLE `wf_cc_order` (
  `order_Id` varchar(32) DEFAULT NULL COMMENT '流程实例ID',
  `actor_Id` varchar(50) DEFAULT NULL COMMENT '参与者ID',
  `creator` varchar(50) DEFAULT NULL COMMENT '发起人',
  `create_Time` varchar(50) DEFAULT NULL COMMENT '抄送时间',
  `finish_Time` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  KEY `IDX_CCORDER_ORDER` (`order_Id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='抄送实例表';

DROP TABLE IF EXISTS `wf_process`;
CREATE TABLE `wf_process` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `name` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `display_Name` varchar(200) DEFAULT NULL COMMENT '流程显示名称',
  `type` varchar(100) DEFAULT NULL COMMENT '流程类型',
  `instance_Url` varchar(200) DEFAULT NULL COMMENT '实例url',
  `state` tinyint(1) DEFAULT NULL COMMENT '流程是否可用',
  `content` longblob COMMENT '流程模型定义',
  `version` int(2) DEFAULT NULL COMMENT '版本',
  `create_Time` varchar(50) DEFAULT NULL COMMENT '创建时间',
  `creator` varchar(50) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_PROCESS_NAME` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='流程定义表';

DROP TABLE IF EXISTS `wf_hist_order`;
CREATE TABLE `wf_hist_order` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `process_Id` varchar(32) NOT NULL COMMENT '流程定义ID',
  `order_State` tinyint(1) NOT NULL COMMENT '状态',
  `creator` varchar(50) DEFAULT NULL COMMENT '发起人',
  `create_Time` varchar(50) NOT NULL COMMENT '发起时间',
  `end_Time` varchar(50) DEFAULT NULL COMMENT '完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '期望完成时间',
  `priority` tinyint(1) DEFAULT NULL COMMENT '优先级',
  `parent_Id` varchar(32) DEFAULT NULL COMMENT '父流程ID',
  `order_No` varchar(50) DEFAULT NULL COMMENT '流程实例编号',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_HIST_ORDER_PROCESSID` (`process_Id`) USING BTREE,
  KEY `IDX_HIST_ORDER_NO` (`order_No`) USING BTREE,
  KEY `FK_HIST_ORDER_PARENTID` (`parent_Id`) USING BTREE,
  CONSTRAINT `wf_hist_order_ibfk_1` FOREIGN KEY (`parent_Id`) REFERENCES `wf_hist_order` (`id`),
  CONSTRAINT `wf_hist_order_ibfk_2` FOREIGN KEY (`process_Id`) REFERENCES `wf_process` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='历史流程实例表';


DROP TABLE IF EXISTS `wf_hist_task`;
CREATE TABLE `wf_hist_task` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `order_Id` varchar(32) NOT NULL COMMENT '流程实例ID',
  `task_Name` varchar(100) NOT NULL COMMENT '任务名称',
  `display_Name` varchar(200) NOT NULL COMMENT '任务显示名称',
  `task_Type` tinyint(1) NOT NULL COMMENT '任务类型',
  `perform_Type` tinyint(1) DEFAULT NULL COMMENT '参与类型',
  `task_State` tinyint(1) NOT NULL COMMENT '任务状态',
  `operator` varchar(50) DEFAULT NULL COMMENT '任务处理人',
  `create_Time` varchar(50) NOT NULL COMMENT '任务创建时间',
  `finish_Time` varchar(50) DEFAULT NULL COMMENT '任务完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '任务期望完成时间',
  `action_Url` varchar(200) DEFAULT NULL COMMENT '任务处理url',
  `parent_Task_Id` varchar(32) DEFAULT NULL COMMENT '父任务ID',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_HIST_TASK_ORDER` (`order_Id`) USING BTREE,
  KEY `IDX_HIST_TASK_TASKNAME` (`task_Name`) USING BTREE,
  KEY `IDX_HIST_TASK_PARENTTASK` (`parent_Task_Id`) USING BTREE,
  CONSTRAINT `wf_hist_task_ibfk_1` FOREIGN KEY (`order_Id`) REFERENCES `wf_hist_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='历史任务表';

DROP TABLE IF EXISTS `wf_hist_task_actor`;
CREATE TABLE `wf_hist_task_actor` (
  `task_Id` varchar(32) NOT NULL COMMENT '任务ID',
  `actor_Id` varchar(50) NOT NULL COMMENT '参与者ID',
  KEY `IDX_HIST_TASKACTOR_TASK` (`task_Id`) USING BTREE,
  CONSTRAINT `wf_hist_task_actor_ibfk_1` FOREIGN KEY (`task_Id`) REFERENCES `wf_hist_task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='历史任务参与者表';


DROP TABLE IF EXISTS `wf_order`;
CREATE TABLE `wf_order` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `parent_Id` varchar(32) DEFAULT NULL COMMENT '父流程ID',
  `process_Id` varchar(32) NOT NULL COMMENT '流程定义ID',
  `creator` varchar(50) DEFAULT NULL COMMENT '发起人',
  `create_Time` varchar(50) NOT NULL COMMENT '发起时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '期望完成时间',
  `last_Update_Time` varchar(50) DEFAULT NULL COMMENT '上次更新时间',
  `last_Updator` varchar(50) DEFAULT NULL COMMENT '上次更新人',
  `priority` tinyint(1) DEFAULT NULL COMMENT '优先级',
  `parent_Node_Name` varchar(100) DEFAULT NULL COMMENT '父流程依赖的节点名称',
  `order_No` varchar(50) DEFAULT NULL COMMENT '流程实例编号',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  `version` int(3) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_ORDER_PROCESSID` (`process_Id`) USING BTREE,
  KEY `IDX_ORDER_NO` (`order_No`) USING BTREE,
  KEY `FK_ORDER_PARENTID` (`parent_Id`) USING BTREE,
  CONSTRAINT `wf_order_ibfk_1` FOREIGN KEY (`parent_Id`) REFERENCES `wf_order` (`id`),
  CONSTRAINT `wf_order_ibfk_2` FOREIGN KEY (`process_Id`) REFERENCES `wf_process` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='流程实例表';



DROP TABLE IF EXISTS `wf_surrogate`;
CREATE TABLE `wf_surrogate` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `process_Name` varchar(100) DEFAULT NULL COMMENT '流程名称',
  `operator` varchar(50) DEFAULT NULL COMMENT '授权人',
  `surrogate` varchar(50) DEFAULT NULL COMMENT '代理人',
  `odate` varchar(64) DEFAULT NULL COMMENT '操作时间',
  `sdate` varchar(64) DEFAULT NULL COMMENT '开始时间',
  `edate` varchar(64) DEFAULT NULL COMMENT '结束时间',
  `state` tinyint(1) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_SURROGATE_OPERATOR` (`operator`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='委托代理表';

DROP TABLE IF EXISTS `wf_task`;
CREATE TABLE `wf_task` (
  `id` varchar(32) NOT NULL COMMENT '主键ID',
  `order_Id` varchar(32) NOT NULL COMMENT '流程实例ID',
  `task_Name` varchar(100) NOT NULL COMMENT '任务名称',
  `display_Name` varchar(200) NOT NULL COMMENT '任务显示名称',
  `task_Type` tinyint(1) NOT NULL COMMENT '任务类型',
  `perform_Type` tinyint(1) DEFAULT NULL COMMENT '参与类型',
  `operator` varchar(50) DEFAULT NULL COMMENT '任务处理人',
  `create_Time` varchar(50) DEFAULT NULL COMMENT '任务创建时间',
  `finish_Time` varchar(50) DEFAULT NULL COMMENT '任务完成时间',
  `expire_Time` varchar(50) DEFAULT NULL COMMENT '任务期望完成时间',
  `action_Url` varchar(200) DEFAULT NULL COMMENT '任务处理的url',
  `parent_Task_Id` varchar(32) DEFAULT NULL COMMENT '父任务ID',
  `variable` varchar(2000) DEFAULT NULL COMMENT '附属变量json存储',
  `version` tinyint(1) DEFAULT NULL COMMENT '版本',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `IDX_TASK_ORDER` (`order_Id`) USING BTREE,
  KEY `IDX_TASK_TASKNAME` (`task_Name`) USING BTREE,
  KEY `IDX_TASK_PARENTTASK` (`parent_Task_Id`) USING BTREE,
  CONSTRAINT `wf_task_ibfk_1` FOREIGN KEY (`order_Id`) REFERENCES `wf_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='任务表';

DROP TABLE IF EXISTS `wf_task_actor`;
CREATE TABLE `wf_task_actor` (
  `task_Id` varchar(32) NOT NULL COMMENT '任务ID',
  `actor_Id` varchar(50) NOT NULL COMMENT '参与者ID',
  KEY `IDX_TASKACTOR_TASK` (`task_Id`) USING BTREE,
  CONSTRAINT `wf_task_actor_ibfk_1` FOREIGN KEY (`task_Id`) REFERENCES `wf_task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='任务参与者表';

DROP TABLE IF EXISTS `wf_workitem`;
CREATE TABLE `wf_workitem` (
  `task_id` varchar(255) NOT NULL,
  `process_id` varchar(255) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `order_no` varchar(255) DEFAULT NULL,
  `process_name` varchar(255) DEFAULT NULL,
  `instance_url` varchar(255) DEFAULT NULL,
  `parent_id` varchar(255) DEFAULT NULL,
  `creator` varchar(255) DEFAULT NULL,
  `order_create_time` varchar(255) DEFAULT NULL,
  `order_expire_time` varchar(255) DEFAULT NULL,
  `order_variable` varchar(255) DEFAULT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `task_key` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `task_create_time` varchar(255) DEFAULT NULL,
  `task_end_time` varchar(255) DEFAULT NULL,
  `task_expire_time` varchar(255) DEFAULT NULL,
  `action_url` varchar(255) DEFAULT NULL,
  `task_type` int(11) DEFAULT NULL,
  `perform_type` int(11) DEFAULT NULL,
  `task_variable` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`task_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

ALTER TABLE uk_consult_invite ADD displayname varchar(50) DEFAULT NULL COMMENT '显示在访客端的用户名';
ALTER TABLE uk_agentservice ADD transagent tinyint(4) DEFAULT 0 COMMENT '是否转人工（1转人工）';

ALTER TABLE uk_chat_message ADD createdate varchar(50) DEFAULT 'null' COMMENT '创建时间';

ALTER TABLE uk_crm_datamodel ADD tiptext varchar(255) DEFAULT NULL COMMENT '提醒内容';
ALTER TABLE uk_databasetask MODIFY lastupdate datetime DEFAULT null COMMENT '创建时间';

ALTER TABLE uk_contacts ADD cbirthday varchar(50) DEFAULT null COMMENT '生日';

ALTER TABLE uk_callcenter_siptrunk ADD prefixstrreg varchar(255) DEFAULT NULL COMMENT '加拨前缀规则';
INSERT INTO `uk_sysdic` VALUES ('4028801e697af58c01697b6afe3b0299', '标签类型', 'pub', 'codetype', 'ukewo', 'layui-icon', '402888815e097729015e0999f26e0002', '', NULL, '', '', NULL, '4028cac3614cd2f901614cf8be1f0324', '2019-3-14 16:56:43', NULL, 1, 0, '402888815e097729015e0999f26e0002', 0, 0, NULL, NULL, NULL, NULL, NULL);


ALTER TABLE uk_historyreport ADD ipaddr varchar(200) DEFAULT null COMMENT '执行任务的IP地址';

ALTER TABLE uk_columnproperties ADD sort tinyint(4) DEFAULT 0 COMMENT '是否允许排序';
ALTER TABLE uk_columnproperties ADD fixed tinyint(4) DEFAULT 0 COMMENT '固定列（left、right）';
ALTER TABLE uk_columnproperties ADD type varchar(50) DEFAULT null COMMENT '列类型'; 

ALTER TABLE uk_columnproperties ADD minwidth varchar(50) DEFAULT null COMMENT '最小宽度';


CREATE TABLE `uk_weixinmenu` (
                               `id` varchar(32) NOT NULL COMMENT '主键ID',
                               `snsid` varchar(32) DEFAULT NULL COMMENT 'SNSID',
                               `parentid` varchar(32) DEFAULT NULL COMMENT '父菜单id',
                               `type` varchar(100) DEFAULT NULL COMMENT '菜单类型',
                               `name` varchar(50) DEFAULT NULL COMMENT '一级菜单最多4个汉字，二级菜单最多7个汉字',
                               `keyv` varchar(255) DEFAULT NULL COMMENT 'click等点击类型必须	菜单KEY值，用于消息接口推送，不超过128字节',
                               `url` varchar(255) DEFAULT NULL COMMENT 'view、miniprogram类型必须	网页 链接，用户点击菜单可打开链接，不超过1024字节。 type为miniprogram时',
                               `mediaid` varchar(255) DEFAULT NULL COMMENT 'media_id	media_id类型和view_limited类型必须	调用新增永久素材接口返回的合法me',
                               `appid` varchar(255) DEFAULT NULL COMMENT 'appid	miniprogram类型必须	小程序的appid（仅认证公众号可配置）',
                               `pagepath` varchar(255) DEFAULT NULL COMMENT 'pagepath	miniprogram类型必须	小程序的页面路径',
                               `sortindex` int(10) DEFAULT NULL COMMENT '排序',
                               `orgi` varchar(32) DEFAULT NULL COMMENT '租户ID',
                               `createtime` datetime DEFAULT NULL COMMENT '创建时间',
                               `creater` varchar(32) DEFAULT NULL COMMENT '创建人',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='微信自定义菜单表';

ALTER TABLE uk_columnproperties ADD dbtableid varchar(32) DEFAULT null COMMENT '元数据id'; 

ALTER TABLE uk_sessionconfig ADD transtip tinyint(4) DEFAULT 0 COMMENT '启用转接提示'; 
ALTER TABLE uk_sessionconfig ADD transtiptitle varchar(100) DEFAULT NULL COMMENT '启用转接提示标题'; 
ALTER TABLE uk_sessionconfig ADD transtipmsg varchar(255) DEFAULT NULL COMMENT '启用转接提示默认内容'; 
ALTER TABLE uk_sessionconfig ADD transtiptimeout int(11) DEFAULT 5000 COMMENT '转接提示默认超时时长（毫秒）'; 

ALTER TABLE uk_user ADD bindext tinyint(4) DEFAULT 0 COMMENT '坐席启用分机绑定'; 
ALTER TABLE uk_user ADD hostid varchar(32) DEFAULT NULL COMMENT '坐席绑定的平台ID'; 
ALTER TABLE uk_user ADD extid varchar(32) DEFAULT NULL COMMENT '坐席绑定的分机ID'; 
ALTER TABLE uk_user ADD extno varchar(32) DEFAULT NULL COMMENT '坐席绑定的分机号码';

ALTER TABLE uk_callcenter_extention ADD userid varchar(32) DEFAULT NULL COMMENT '分机绑定的坐席ID';
ALTER TABLE uk_callcenter_extention ADD uname varchar(50) DEFAULT NULL COMMENT '分机绑定的坐席姓名';
ALTER TABLE uk_callcenter_extention ADD username varchar(50) DEFAULT NULL COMMENT '分机绑定的坐席用户名';

ALTER TABLE uk_tableproperties ADD sort tinyint(4) DEFAULT 0 COMMENT '是否允许排序';
ALTER TABLE uk_tableproperties ADD fixed tinyint(4) DEFAULT 0 COMMENT '固定列（left、right）';
ALTER TABLE uk_tableproperties ADD width varchar(50) DEFAULT null COMMENT '宽度';
ALTER TABLE uk_tableproperties ADD minwidth varchar(50) DEFAULT null COMMENT '最小宽度';
ALTER TABLE uk_tableproperties ADD alignment varchar(255) DEFAULT NULL COMMENT '对齐方式';
ALTER TABLE uk_tableproperties ADD unresize tinyint(4) DEFAULT 0 COMMENT '是否禁用拖拽列宽';

ALTER TABLE uk_columnproperties ADD datatypename varchar(255) DEFAULT NULL COMMENT '字段类型名称';
ALTER TABLE uk_columnproperties ADD unresize tinyint(4) DEFAULT 0 COMMENT '是否禁用拖拽列宽';

ALTER TABLE uk_columnproperties ADD seldatacode varchar(255) DEFAULT NULL COMMENT '字典项';
ALTER TABLE uk_columnproperties ADD seldata tinyint(4) DEFAULT 0 COMMENT '是否启用字典';

ALTER TABLE uk_columnproperties ADD required tinyint(4) DEFAULT 0 COMMENT '是否必填';
ALTER TABLE uk_columnproperties ADD readonly tinyint(4) DEFAULT 0 COMMENT '是否只读';
ALTER TABLE uk_columnproperties ADD placeholder varchar(255) DEFAULT NULL COMMENT '占位符';
ALTER TABLE uk_crm_datamodel ADD sorttype varchar(255) DEFAULT NULL COMMENT '排列方式';
ALTER TABLE uk_crm_datamodel ADD sortcol int(11) DEFAULT 1 COMMENT '列数';


ALTER TABLE uk_agentservice ADD transagenttime datetime DEFAULT NULL COMMENT '转人工坐席时间';
ALTER TABLE uk_agentservice ADD transtraceid varchar(32) DEFAULT NULL COMMENT '转人工坐席服务ID';
ALTER TABLE uk_agentservice ADD fromai tinyint(4) DEFAULT 0 COMMENT '是否来自于机器人服务';

ALTER TABLE uk_agentservice ADD aiserviceid varchar(32) DEFAULT NULL COMMENT '机器人服务ID';

ALTER TABLE uk_crm_datamodel ADD actiontypefield varchar(50) DEFAULT NULL COMMENT '布局位置：单元格字段名';

ALTER TABLE uk_systemconfig ADD enableoss TINYINT(4) DEFAULT 0 COMMENT '是否启用对象存储OSS';
ALTER TABLE uk_callcenter_event ADD ossstatus varchar(4) DEFAULT '0' COMMENT '//录音文件是否上传到oss 0否 1是 2文件不存在';


ALTER TABLE uk_systemconfig ADD cloudserverupdatecheckurl VARCHAR(255) DEFAULT NULL COMMENT '连接服务端-更新检查-url';
ALTER TABLE uk_systemconfig ADD cloudserveruploadossurl VARCHAR(255) DEFAULT NULL COMMENT '连接服务端-上传到oss-url';
ALTER TABLE uk_systemconfig ADD cloudservergetosssizeurl VARCHAR(255) DEFAULT NULL COMMENT '连接服务端-获取客户oss容量-url';
ALTER TABLE uk_systemconfig ADD cloudservergetossobjecturl VARCHAR(255) DEFAULT NULL COMMENT '连接服务端-获取oss对象-url';

ALTER TABLE uk_agentuser ADD transfer tinyint DEFAULT 0 COMMENT '是否在转接状态';
ALTER TABLE uk_agentuser ADD transfertime datetime DEFAULT NULL COMMENT '转接超时时间';

ALTER TABLE uk_chat_message ADD debug tinyint(4) DEFAULT 0 COMMENT '开发模式';


ALTER TABLE uk_callcenter_event ADD coreuuid varchar(50) DEFAULT NULL COMMENT 'FS标识';

ALTER TABLE uk_xiaoe_sceneitem ADD inputcon text DEFAULT NULL COMMENT '输入条件';
ALTER TABLE uk_xiaoe_sceneitem ADD outputcon text DEFAULT NULL COMMENT '输出条件';

CREATE INDEX index_101 ON uk_chat_message  ('contextid') USING BTREE;
CREATE INDEX index_102 ON uk_chat_message  ('orgi') USING BTREE;
CREATE INDEX index_103 ON uk_chat_message  ('updatetime') USING BTREE;
CREATE INDEX index_200 ON uk_callcenter_event  (`code`) USING BTREE;
CREATE INDEX index_201 ON uk_callcenter_event  (`datestr`) USING BTREE;
CREATE INDEX index_202 ON uk_callcenter_event  (`hourstr`) USING BTREE;
CREATE INDEX index_203 ON uk_callcenter_event  (`contactsid`) USING BTREE;
CREATE INDEX index_204 ON uk_callcenter_event  (`orgi`) USING BTREE;
CREATE INDEX index_205 ON uk_callcenter_event  (`transtatus`) USING BTREE;
CREATE INDEX index_206 ON uk_callcenter_event  (`transbegin`) USING BTREE;
CREATE INDEX index_207 ON uk_callcenter_event  (`servicestatus`) USING BTREE;
CREATE INDEX index_208 ON uk_callcenter_event  (`createtime`) USING BTREE;
CREATE INDEX index_209 ON uk_callcenter_event  (`starttime`) USING BTREE;
CREATE INDEX index_210 ON uk_callcenter_event  (`discalled`) USING BTREE;
CREATE INDEX index_211 ON uk_callcenter_event  (`discaller`) USING BTREE;

ALTER TABLE `uk_system_updatecon`
	MODIFY COLUMN `jarurl` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'war包下载' ,
	MODIFY COLUMN `sqlurl` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '脚本下载url';
